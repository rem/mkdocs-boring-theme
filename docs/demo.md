---
title: Demo Page
---

# Demo Headlines

## Headline 2

### Headline 3

#### Headline 4

##### Headline 5

###### Headline 6

# Demo

## Horizontal rules

Before the horizontal rule

---

After the horizontal rule


## Paragraphs

This is some regular text. Here is some **bold text**. Also some *italics text*.
What about the ***combination of both***?


## Lists


### Ordered Lists

1. First item
2. Second item
3. Third item


### Unordered Lists

* First item
* Second item
* Third item


## Blockquotes

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
> nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.


### Nested Blockquotes

> Some nested blockquote
>
>> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
>> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
>> nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.


## Code blocks

This is `inline code` formatting.

```
int main() {
  /* some code block */
  return 0;
}
```


