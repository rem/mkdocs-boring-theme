---
title: Release Notes
---

# Release Notes

## Version 0.1.10

* 2020-12-18

Features: Minor changes in text alignments, justified paragraph nodes, centered headlines


## Version 0.1.7

* 2020-12-16

Features: Updated text decoration in navigation bar links. Integrated new color palette. Display line numbering in code blocks.


## Version 0.1.3

* 2020-12-04

Initial commit.

Features: grid layout, navigation, page navigation, navigation bar, search field, 404 page, blockquotes, code blocks, repo link, copyright notice

Misc: Documentation, release notes
