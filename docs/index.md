# MkDocs Boring Theme

This is the MkDocs Boring Theme.
It is based on the CSS framework [Min](http://mincss.com) which claims to be one of the slimmest but also one of the most compatible CSS frameworks.

Check out the demo page to get a feeling of this theme.


## Goal

The goal is to provide a lightweight theme which still does not look too awkward.
*Min* promotes itself as being only 995 bytes, which is hard to challenge.

It's ultimately small size requires a few overwriting features which can be found in `css/boring.css`.

## Install & Usage

The boring theme is packaged and available on PyPI:

```
pip install mkdocs-boring-theme
```

If you want to use the theme in your MkDocs project simply include it in the `mkdocs.yml`:

```
theme:
  name: boring

```

## Configuration

The theme allows the usage of two new configuration parameters:

```
provider_name: Example
provider_url: https://example.com
```

The provider will be shown in the top-left corner which can link to the providers home page.



## Todo List

This theme is a work in progress. It lacks a few features, which I try to list here

* Nested menus with dropdown effects
* Minimizing `css/boring.css`

